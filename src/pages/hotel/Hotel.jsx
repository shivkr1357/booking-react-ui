import "./hotel.css";
import Navbar from "../../components/navbar/Navbar";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import MailList from "../../components/mailList/MailList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLocationDot,
  faCircleArrowLeft,
  faCircleArrowRight,
} from "@fortawesome/free-solid-svg-icons";
// import { faLocationDot } from "@fortawesome/free-regular-svg-icons";
import image1 from "../../assets/02.jpg";
import image2 from "../../assets/03.jpg";
import image3 from "../../assets/04.jpg";
import image4 from "../../assets/05.jpg";
import image5 from "../../assets/06.jpg";
import image6 from "../../assets/home-insurance-e1479125215618.jpg";
import { useState } from "react";
import { faCircleXmark } from "@fortawesome/free-regular-svg-icons";

const Hotel = () => {
  const [slideNumber, setSlideNumber] = useState(0);
  const [open, setOpen] = useState(false);

  const photos = [
    { src: image1 },
    { src: image2 },
    { src: image3 },
    { src: image4 },
    { src: image5 },
    { src: image6 },
  ];

  const handleOpen = (i) => {
    setSlideNumber(i);
    setOpen(true);
  };

  const handleMove = (direction) => {
    let newSlideNumber;

    if (direction === "l") {
      newSlideNumber = slideNumber === 0 ? 5 : slideNumber - 1;
    } else {
      newSlideNumber = slideNumber === 5 ? 0 : slideNumber + 1;
    }

    setSlideNumber(newSlideNumber);
  };

  return (
    <div>
      <Navbar />
      <Header type="list" />
      <div className="hotelContainer">
        {open && (
          <div className="slider">
            <FontAwesomeIcon
              icon={faCircleXmark}
              className="close"
              onClick={() => setOpen(false)}
            />
            <FontAwesomeIcon
              icon={faCircleArrowLeft}
              className="arrow"
              onClick={() => handleMove("l")}
            />
            <div className="sliderWrapper">
              <img src={photos[slideNumber].src} alt="" className="sliderImg" />
            </div>
            <FontAwesomeIcon
              icon={faCircleArrowRight}
              className="arrow"
              onClick={() => handleMove("r")}
            />
          </div>
        )}
        <div className="hotelWrapper">
          <button className="bookNow">Reserve or Book Now</button>
          <h1 className="hotelTitle"> Grand Hotel</h1>
          <div className="hotelAddress">
            <FontAwesomeIcon icon={faLocationDot} />
            <span>Muzaffarpur Bihar 843127</span>
          </div>
          <span className="hotelDistance">
            Excellent Location - 500 mtrs from center
          </span>
          <span className="hotelPriceHighlight">
            Book a stay for $120 and get free airport taxi
          </span>
          <div className="hotelImages">
            {photos.map((photo, i) => (
              <div className="hotelImageWrapper">
                <img
                  onClick={() => handleOpen(i)}
                  src={photo.src}
                  alt=""
                  className="hotelImage"
                />
              </div>
            ))}
          </div>
          <div className="hotelDetails">
            <div className="hotelDetailTexts">
              <h1 className="hotelTitle">Stay in the Smart City</h1>
              <p className="hotelDesc">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum
                magnam voluptas atque ut fugit voluptatum. Dolorem possimus,
                dolores, odio itaque iusto corporis error nemo quibusdam porro
                aspernatur accusamus nisi commodi eius ad. Dolorem illo corporis
                eius consectetur. Accusantium quod laborum molestias, quos
                perspiciatis voluptas, consequatur voluptate non impedit qui
                sed?
              </p>
            </div>
            <div className="hotelDetailPrice">
              <h1>Perfect for a 9night stay !</h1>
              <span>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis
                laudantium aut reiciendis, provident nesciunt voluptatum.
              </span>
              <h2>
                <b>$999</b> (9 nights)
              </h2>
              <button>Reserve or book now</button>
            </div>
          </div>
        </div>
        <MailList />
        <Footer />
      </div>
    </div>
  );
};

export default Hotel;

import React from "react";
import "./featured.css";
import image from "../../assets/05.jpg";
import image1 from "../../assets/02.jpg";
import image2 from "../../assets/03.jpg";
import image3 from "../../assets/04.jpg";
const Featured = () => {
  return (
    <div className="featured">
      <div className="featuredItems">
        <img src={image} alt="" className="featuredImage" />
        <div className="featuredTitles">
          <h1>Mumbai</h1>
          <h2>123 Properties</h2>
        </div>
      </div>
      <div className="featuredItems">
        <img src={image1} alt="" className="featuredImage" />
        <div className="featuredTitles">
          <h1>Delhi</h1>
          <h2>234 Properties</h2>
        </div>
      </div>
      <div className="featuredItems">
        <img src={image2} alt="" className="featuredImage" />
        <div className="featuredTitles">
          <h1>Chennai</h1>
          <h2>345 Properties</h2>
        </div>
      </div>
      <div className="featuredItems">
        <img src={image3} alt="" className="featuredImage" />
        <div className="featuredTitles">
          <h1>Kolkata</h1>
          <h2>456 Properties</h2>
        </div>
      </div>
    </div>
  );
};

export default Featured;

import React from "react";
import "./featuredProperties.css";
import image from "../../assets/05.jpg";

const FeaturedProperties = () => {
  return (
    <div className="fp">
      <div className="fpItem">
        <img src={image} alt="" className="fpImg" />
        <span className="fpName">Apparment Mumbai Fiasco</span>
        <span className="fpCity">Mumbai</span>
        <span className="fpPrice">Starting from $100</span>
        <div className="fpRating">
          <button>5.5</button>
          <span>Excellent</span>
        </div>
      </div>
      <div className="fpItem">
        <img src={image} alt="" className="fpImg" />
        <span className="fpName">Apparment Mumbai Fiasco</span>
        <span className="fpCity">Mumbai</span>
        <span className="fpPrice">Starting from $100</span>
        <div className="fpRating">
          <button>5.5</button>
          <span>Excellent</span>
        </div>
      </div>
      <div className="fpItem">
        <img src={image} alt="" className="fpImg" />
        <span className="fpName">Apparment Mumbai Fiasco</span>
        <span className="fpCity">Mumbai</span>
        <span className="fpPrice">Starting from $100</span>
        <div className="fpRating">
          <button>5.5</button>
          <span>Excellent</span>
        </div>
      </div>
      <div className="fpItem">
        <img src={image} alt="" className="fpImg" />
        <span className="fpName">Apparment Mumbai Fiasco</span>
        <span className="fpCity">Mumbai</span>
        <span className="fpPrice">Starting from $100</span>
        <div className="fpRating">
          <button>5.5</button>
          <span>Excellent</span>
        </div>
      </div>
    </div>
  );
};

export default FeaturedProperties;

import React from "react";
import "./propertyList.css";
import image from "../../assets/02.jpg";

const PropertyList = () => {
  return (
    <div className="pList">
      <div className="pListItem">
        <img src={image} alt="" className="pListImg" />
        <div className="pListTitles">
          <h1>Hotels</h1>
          <h2>233 Mumbai</h2>
        </div>
      </div>
      <div className="pListItem">
        <img src={image} alt="" className="pListImg" />
        <div className="pListTitles">
          <h1>Hotels</h1>
          <h2>233 Mumbai</h2>
        </div>
      </div>
      <div className="pListItem">
        <img src={image} alt="" className="pListImg" />
        <div className="pListTitles">
          <h1>Hotels</h1>
          <h2>233 Mumbai</h2>
        </div>
      </div>
      <div className="pListItem">
        <img src={image} alt="" className="pListImg" />
        <div className="pListTitles">
          <h1>Hotels</h1>
          <h2>233 Mumbai</h2>
        </div>
      </div>
      <div className="pListItem">
        <img src={image} alt="" className="pListImg" />
        <div className="pListTitles">
          <h1>Hotels</h1>
          <h2>233 Mumbai</h2>
        </div>
      </div>
    </div>
  );
};

export default PropertyList;

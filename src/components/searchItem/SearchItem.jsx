import "./searchItem.css";
import image from "../../assets/02.jpg";

const SearchItem = () => {
  return (
    <div className="searchItems">
      <img src={image} alt="" className="siImage" />
      <div className="siDesc">
        <h1 className="siTitle">Tower Street Appartment</h1>
        <span className="siDistance">500mtrs from center</span>
        <span className="siTaxiOp">Free Airport Taxi</span>
        <span className="siSubtitle">
          Studio Appartmet with Air conditioner
        </span>
        <span className="siFeatures">
          Entire Studio * 1 bathroom 21m2 1 full bed
        </span>
        <span className="siCancelOp">Free cancellation</span>
        <span className="siCancelOpSubtitle">
          You can cancel later so lock in this price today !
        </span>
      </div>
      <div className="siDetails">
        <div className="siRating">
          <span>Excellent</span>
          <button>8.9</button>
        </div>
        <div className="siDetailTexts">
          <span className="siPrice">$120</span>
          <span className="siTaxOp">Including Taxes and fees</span>
          <button className="siCheckButton">See Availability</button>
        </div>
      </div>
    </div>
  );
};

export default SearchItem;
